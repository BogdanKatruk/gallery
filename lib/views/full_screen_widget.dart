import 'package:flutter/material.dart';
import 'package:gallery/misc/colors.dart';
import 'package:gallery/misc/interface_image.dart';
//import 'package:pinch_zoom_image/pinch_zoom_image.dart';

class FullScreenWidget extends StatelessWidget {
  FullScreenWidget({Key key, this.image});
  final IImage image;
  static const String routeName = 'fullScreenWidget';

  @override
  Widget build(BuildContext context) {
    IImage _args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.PrimaryColor,
          centerTitle: true,
          title: Image.asset('assets/images/unsplash_logo.png', scale: 8.5),
          leading: InkWell(
              child: Icon(Icons.arrow_back),
              onTap: () => Navigator.pop(context)
          ),
        ),
        body:
        Container(
          height: MediaQuery.of(context).size.height,
          child:
   // PinchZoomImage(
       //image:
            Image.network(_args.fullImgUrl, height: MediaQuery.of(context).size.height,width: MediaQuery.of(context).size.width,fit: BoxFit.contain),
        )
      // Container(
      //   child: Image.network(image.fullImgUrl),
      // )
    );
  }
}
