abstract class IImage {
  String get authorName;
  String get fullImgUrl;
  String get authorAvatar;
  String get imgId;
}
