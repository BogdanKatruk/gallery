import 'package:flutter/material.dart';
import 'package:gallery/views/full_screen_widget.dart';
import 'package:gallery/views/main_gallery_widget.dart';


void main() {
  runApp(MaterialApp(initialRoute: GalleryWidget.routeName,
      debugShowCheckedModeBanner: false,
      routes: {
        GalleryWidget.routeName: (context) => GalleryWidget(),
        FullScreenWidget.routeName: (context) => FullScreenWidget(),
      }));
}
